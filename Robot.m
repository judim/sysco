classdef Robot < handle
  methods
    function obj = Robot(path)
      if and(nargin >= 1, ischar(path))
        if exist(path)
          obj.props = jsondecode(fileread(path));
        else
          error('Model file does not exist')
        end
      else
        error('Value must be valid path.')
      end

      body = obj.props.body;
      wheel = obj.props.wheel;

      wheel.inertia = wheel.mass * wheel.radius^2 / 2.0;
      body.trunk = body.height / 2.0;
      body.pitchInertia = body.mass * body.trunk^2 / 3.0;
      body.yawInertia = body.mass * (body.width^2 + body.depth^2)/ 12.0;

      obj.props.body = body;
      obj.props.wheel = wheel;

      obj.A = zeros(6, 6)
      obj.B = zeros(6, 2)
      obj.C = eye(6, 6)

    end 
    
    function computeMatrices(obj)
      body = obj.props.body;
      wheel = obj.props.wheel;
      motor = obj.props.motor;

      K = 0.5 * wheel.mass * body.width^2 + body.yawInertia;
      K = K + body.width^2 / 2.0 / wheel.radius^2 ... 
            * ( wheel.inertia + motor.gearratio^2 * motor.inertia );
     
      alpha = motor.gearratio * motor.torque / motor.resistance;

      beta = (motor.gearratio * motor.torque * motor.emf) / motor.resistance  ...
           + motor.bmfriction;
      I = beta + body.width / wheel.radius * motor.wffriction
      J = wheel.radius / body.width * alpha;

      E = zeros(2)
      E(1, 1) = (2.0 * wheel.mass + body.mass) * wheel.radius^2 ...
              + 2 * wheel.inertia + 2 * motor.gearratio^2 * motor.inertia;
      E(1, 2) = body.mass * body.trunk * wheel.radius ...
              - 2 * motor.gearratio^2 * motor.inertia;
      E(2, 1) = E(1, 2);
      E(2, 2) = body.mass * body.trunk^2 + body.pitchInertia ...
              + 2.0 * motor.gearratio^2 * motor.inertia;

      F = zeros(2);
      F(1, 1) = beta + motor.wffriction;
      F(1, 2) = - beta;
      F(2, 1) = - 2.0 * beta;
      F(2, 2) = 2 * beta;
     
      phys.g = 9.81; 
      G = zeros(2);
      G(2, 2) = - body.mass * phys.g * body.trunk;
      
      H = alpha * ones(2);
      H(1, :) = H(1, :) / 2.0;
      H(2, :) = - H(2, :);

      obj.A(5:6, 5:6) = [0, 1; ...
                         0, -I/K];
      obj.A(1:2, 3:4) = eye(2);
      obj.A(3:4, 1:2) = - inv(E) * G;
      obj.A(3:4, 3:4) = - inv(E) * F;

      obj.B(3:4,:) = inv(E) * H;
      obj.B(5:6, 1:2) = [0, 0; ...
                        -J/K, J/K];


    end
  end
  
  properties
    props = {}
    A = []
    B = []
    C = []
  end
end
